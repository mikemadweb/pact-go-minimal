package main

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
)

func TestPactProvider(t *testing.T) {
	var port, _ = utils.GetFreePort()
	var address = fmt.Sprintf("0.0.0.0:%d", port)
	go startApiNotWorking(address)

	currentDir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}
	pact := dsl.Pact{
		Provider: "api",
		Consumer: "client",
		LogLevel: "DEBUG",
		LogDir: currentDir + "log/api",
	}
	_, err = pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:    fmt.Sprintf("http://%s", address),
		FailIfNoPactsFound: true,
		PactURLs:           []string{filepath.FromSlash(fmt.Sprintf("%s/client-api.json", currentDir))},
		ProviderVersion:    "1.0.0",
	})

	if err != nil {
		t.Fatalf("Error on Verify: %v", err)
	}
}