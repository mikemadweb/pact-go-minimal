module bitbucket.org/mikemadweb/pact-go-minimal

go 1.13

require (
	github.com/go-playground/validator/v10 v10.3.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/pact-foundation/pact-go v1.4.0
)
