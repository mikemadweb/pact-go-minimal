FROM golang:1.14

ENV GOPATH="/go"
ENV PATH="$GOPATH/bin:/usr/local/go/bin:/pact/bin:$PATH"
ENV TZ="Europe/Luxembourg"

WORKDIR "/"

RUN apt-get update -y -q \
    && apt-get upgrade -y -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y -q \
        curl \
        build-essential \
        ca-certificates \
        git \
        nmap \
    && mkdir -p "$GOPATH/src" "$GOPATH/bin" \
    && chmod +x "$GOPATH" \
    && curl -fsSL https://raw.githubusercontent.com/pact-foundation/pact-ruby-standalone/master/install.sh | bash

WORKDIR "/app"
