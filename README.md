## How to run
```bash
docker build -t minimal . && docker run -v $(pwd):/app -it minimal go test .
```