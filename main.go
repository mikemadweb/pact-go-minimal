package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Request struct {
	Param1 string `json:"param1" validate:"required,numeric"`
	Param2 string `json:"param2" validate:"required,numeric"`
}

func main() {
	startApiNotWorking("0.0.0.0:80")
}

func startApiWorking(address string) {
	e := echo.New()
	e.HideBanner = true

	e.POST("/v1/data", func(context echo.Context) error {
		var request Request
		if err := json.NewDecoder(context.Request().Body).Decode(&request); err != nil {
			return err
		}
		if err := context.Validate(&request); err != nil {
			return err
		}

		return context.JSONBlob(http.StatusOK, []byte(`{"result": 1.1}`))
	})

	e.Use(middleware.Recover())
	e.Validator = NewValidator()

	e.Logger.Fatal(e.Start(address))
}

func startApiNotWorking(address string) {
	e := echo.New()
	e.HideBanner = true
	e.Debug = true

	e.POST("/v1/data", func(context echo.Context) error {
		var request Request
		fmt.Println(context.Request().Header.Get("Content-Type"))
		if err := context.Bind(&request); err != nil {
			return err
		}
		if err := context.Validate(&request); err != nil {
			return err
		}

		return context.JSONBlob(http.StatusOK, []byte(`{"result": 1.1}`))
	})

	e.Use(middleware.Recover())
	e.Validator = NewValidator()

	e.Logger.Fatal(e.Start(address))
}

type requestValidator struct {
	validator *validator.Validate
}

func (v *requestValidator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}

func NewValidator() *requestValidator {
	return &requestValidator{
		validator: validator.New(),
	}
}
